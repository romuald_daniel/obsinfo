# Test routines

I'm following the instructions/suggestions at
https://docs.python-guide.org/writing/structure/

All test files should be here, each named test_something.py

I have NOT YET copied over the files in obsinfo/tests/ because Louis actually
created a command-line file that runs the tests.  Not sure it's a good idea, but
I don't know what to do either.  Also, these are long to run, so maybe they should
be kept separate from the other tests (and maybe the stationxml creation tests
should go in here too)

## Test discovery

`python -m unittest`

## Test file format

Tests are currently using `unittest`, but py.test is recommended so maybe I'll
go there some day.

## Accessing the `obsinfo` module

I tried the technique recommended on the site for accessing modules without
having to install it (creating a context.py file and then writing
`from .context import sample` at the top of each test_*.py file, but got 
the error:

``
  File "test_network.py", line 12, in <module>
      from .context import obsinfo
  ImportError: attempted relative import with no known parent package
``

So for now I'm just assuming that obsinfo is installed