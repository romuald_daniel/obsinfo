#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test StationXML creation
"""
from pathlib import Path
import glob
import unittest
import inspect
# from pprint import pprint
import warnings

from obsinfo.obsMetadata.obsmetadata import ObsMetadata

warnings.simplefilter("once")
warnings.filterwarnings("ignore", category=DeprecationWarning)
verbose = False

     
class obsmetadataTest(unittest.TestCase):
    """
    Test methods for obsmetadata 
    """
    def setUp(self):
        self.testing_path = Path(__file__).parent / "data"
        self.infofiles_path = (Path(__file__).resolve().parents[2]
                               / "_examples" / 'Information_Files')

    def test_valid_types(self):
        """
        Test is_valid_type.
        """
        for filetype in ("author", "datacite", "datalogger", "filter",
                         "instrumentation", "location_base", "network_info",
                         "network", "operator", "preamplifier", "sensor",
                         "stage", "station"):
            self.assertTrue(ObsMetadata.is_valid_type(filetype),
                            msg=f'filetype "{filetype}" is not valid')
        for filetype in ("wabbit", "bollweevil", "cawwot", "ACME"):
            self.assertFalse(ObsMetadata.is_valid_type(filetype))


def suite():
    return unittest.makeSuite(obsmetadataTest, 'test')


if __name__ == '__main__':
    unittest.main(defaultTest='suite')

