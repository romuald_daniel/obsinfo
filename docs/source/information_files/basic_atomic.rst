**************************
Basic atomic - two channel
**************************

Here is an example of a the same information as in basic_flat, divided across the
standard obsinfo file structure.  You can see that, despite the existence of
headers in each file, there are many less lines and less repetition.

The network below is found in
obsinfo/_examples/examples/BASIC_ATOMIC.network.yaml and the other information
files are found in the obsinfo/_examples hierarchy, although any configuration
definitions have been removed here for simplicity.  To see what configuration
definitions can do, see the basic_configuration document. 

.. code-block:: yaml
