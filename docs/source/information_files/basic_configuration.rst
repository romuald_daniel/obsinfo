******************************************
Basic atomic + configuration - two channel
******************************************

Using the same basic instrumentation as above, here is an example of configuration,
which allows us to:

    - change instrument components from the default (sensors, preamplifiers,
      dataloggers)
    - modify parameters of one or more instrument components (serial numbers,
      response stage, datalogger sampling frequency and/or digital filter)

The network below is found in
obsinfo/_examples/examples/BASIC_CONFIGURATION.network.yaml and the other information
files are found in the obsinfo/_examples hierarchy

.. code-block:: yaml
