*****************
Overview
*****************

We present here the hierarchy of information files.  These files are generally
divided into sub-files using the `$ref:` operator inherited from JSONref.  We will
present:
 
1) the "standard" but not required structure of these files
2) A full description of the format, divided into these structures
3) A full, undivided, tree of all possible fields

Source
===================================================

The definition (JSON Schemas) for the information files are in obsinfo/data/schemas

1) Basic level diagram
===================================================

Only required fields are shown, except a few very commonly-used fields,
surrounded by "*"s. atomic objects described below are surrounded by <>

.. code-block:: yaml

    network: <network>
        operator:
            <operator>
        network_info:
            <network_info>
        stations:
            {STATIONNAME1}: <station>
                site: string
                start_date: string
                end_date: string
                location_code: string
                locations:
                    {LOCATION_1}: <location>
                    {LOCATION_2}: <location>
                    ...
                instrumentation: <instrumentation>
                    equipment:
                        <equipment object>
                    channels:
                        default: <channel>
                            datalogger: <datalogger>
                                equipment:
                                    <equipment>
                                sample_rate: number
                                response_stages:
                                    - <stage>
                                        input_units: <units>
                                            name: string
                                            description: string
                                        output_units: <units>
                                        gain:
                                            value: float
                                        filter: <filter>
                                            type: string
                                    - <stage>
                                    - ...
                            *preamplifier: <preamplifier>*
                                equipment:
                                    <equipment>
                                response_stages:
                                    - <stage>
                                    - <stage>
                                    - ...
                            sensor: <sensor>
                                equipment:
                                    <equipment>
                                seed_codes: 
                                    band_base: "B" or "S"
                                    instrument: single character
                                response_stages:
                                    - <stage>    
                                    - <stage>
                                    - ...
                       {SPECIFIC-CHANNEL1}: subset of <channel>
                       {SPECIFIC-CHANNEL2}: subset of <channel>
                       ...
            {STATIONNAME2}:
                ...


2) Atomic level diagram with comments
===================================================

Starred fields are optional. If you put a level in a separate file, it is good
practice to use the following file structure:

.. code-block:: yaml

    format_version: <format_version>
    *revision: <revision>*
    *notes: list of strings*
    level: <level>
    
Major objects
=============
        
``network``
------------------------

.. code-block:: yaml

    operator: <operator>
    *stations_operator: <operator>*
    *restricted_state: {}*
    *comments: list of strings*
    *extras: <free-form object>*
    campaign_ref_name: string
    network_info: <network_info>
    stations:
        <STATIONNAME1>:
            <station>
        <STATIONNAME2>:
            <station>
        ...

``station``
------------------------

.. code-block:: yaml

    site: string
    start_date: string
    end_date: string
    location_code: string
    *serial_number: string*
    *operator: <operator>*
    instrumentation: <instrumentation>
    *channel_modifications: {}*
    locations: object list of <location>
    *notes: list of strings*
    *comments: list of strings or <stationxml_comment>*
    *extras: <free-form object>*
    *processing:*
        - *clock_correction_linear: {}*
        - *clock_correction_leapsecond: {}*

``instrumentation``
------------------------

.. code-block:: yaml

    *operator: {}*
    equipment: {}
    channels:
        default:
            <channel>
       <SPECIFIC-CHANNEL1>:
            subset of <channel>
       <SPECIFIC-CHANNEL2>:
            subset of <channel>
       ...

``channel``
------------------------

.. code-block:: yaml

    *orientation_code: string or {}*
    datalogger:
        <datalogger>
    *preamplifier:*
        <preamplifier>
    sensor:
        <sensor>
    *preamplifier_configuration: string*
    *sensor_configuration: string*
    *datalogger_configuration: string*
    *location_code: string* # otherwise inherits from station
    *comments: list of strings*
    *extras: <free-format object>*

``datalogger``
------------------------

.. code-block:: yaml

        <GENERIC_COMPONENT>
        sample_rate: number
        *delay_correction: number*


``preamplifier``
------------------------

.. code-block:: yaml

        <GENERIC_COMPONENT>


``sensor``
------------------------

.. code-block:: yaml

        <GENERIC_COMPONENT>
         seed_codes:

``GENERIC_COMPONENT`` 
------------------------

.. code-block:: yaml

    equipment: 
        <equipment>
    *configuration_default: string*
    *response_modifications: {}*
    *notes: list of strings*
    *response_stages:*
        - <stage object>
        - <stage object>
        - ...
    *configuration_definitions:*
        {CONFIGURATION_1}: <subset of datalogger, sensor, or preamplifier object>
        {CONFIGURATION_2}: <subset of datalogger, sensor, or preamplifier object>
        ...

``stage``
------------------------

.. code-block:: yaml

    input_units: string
    output_units string
    gain: float
    *name: string*
    *description: string*
    *decimation_factor: integer*
    *delay: number*
    *calibration_date: string*
    *polarity: '+' or '-'*     # default is '+'
    *input_sample_rate: number*
    *filter:*
        <filter>

``filter``
------------------------

fields depend on type:

.. code-block:: yaml

    type: "PoleZeros"
    poles: list
    zeros: list
    *offset: float*
    *transfer_function_type":  "LAPLACE (RADIANS/SECOND)", "LAPLACE (HERTZ)", or "DIGITAL (Z-TRANSFORM)"
    *normalization_frequency: float*
    *normalization_factor:  float*

.. code-block:: yaml

    type: "FIR"
    symmetry:  "EVEN", "ODD" or "NONE"
    coefficients:  list
    offset: float
    "coefficient_divisor":  float

.. code-block:: yaml

    type: "Coefficients"
    numerator_coefficients: list
    denominator_coefficients": list
    *transfer_function_type: "ANALOG (RADIANS/SECOND)", "ANALOG (HERTZ)" or "DIGITAL"*
    *offset: float*

.. code-block:: yaml

    type: "ResponseList"
    elements: list
    *offset: float*

.. code-block:: yaml

    type: "Polynomial"
    # Not implemented, had no examples to verify with

.. code-block:: yaml

    type: "ADConversion"
    input_full_scale: float
    output_full_scale: float

.. code-block:: yaml

    type: "Analog"
 
.. code-block:: yaml

    type: "Digital"

Minor objects
------------------------

author_, operator_, location_base_ and network_info_ are often
in separate files, should probably have their own schema file.

equipment_ is widespread enough that it should probably have its
own schema file as well

author:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    first_name: string
    last_name: string"
    *institution: string*
    *email: string*
    *phones: array*
        
equipment:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    type: string
    description: string
    manufacturer: string
    model: string
    *vendor: string*
    *serial_number: string*
    *installation_date: string, date formatted*
    *removal_date: string, date formatted*
    *calibration_dates: array of strings, date format*

location:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    base:
        uncertainties.m:
            lat: number (in meters)
            lon: number (in meters)
            elev: number (in meters)
        depth.m: number
        geology: string
        vault: string
        *localisation_method: string*
    position:
        lat: number (in degrees)
        lon: number (in degrees)
        elev: number (in meters)
        

network_info:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    code: string
    name: string
    start_date: string, date formatted
    end_date: string, date formatted
    description: string
    *comments: list of strings or <stationxml_comment>*
    *website: string*

operator:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    reference_name: string
    *full_name: string*
    *contact: <author object>*
    *phone_number: string*
    *email: string*
    *website: string*

revision:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    date: string
    authors: list of <author object>

stationxml_comment:
~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    value: string
    *begin_effective_time: string, date formatted*
    *end_effective_time: string, date formatted*
    *authors: list of <author>*


3) Full level diagram
===================================================

Structural units
===================================================

A full ``obsinfo`` network description consists of the following fields
(starred fields are optional):

.. code-block:: yaml

    format_version: {}
    *revision: {}*
    *notes: []*
    network:
        operator: {}
        *stations_operator: {}*
        *restricted_state: {}*
        *comments: []*
        *extras: {}*
        campaign_ref_name: string
        network_info: {}
        stations:
            <STATIONNAME1>:
                site: string
                start_date: string
                end_date: string
                location_code: string
                *serial_number: string*
                *operator: {}*
                instrumentation:
                    *operator: {}*
                    equipment: {}
                    channels:
                        default:
                            *orientation_code: string or {}*
                            datalogger:
                                << GENERIC_COMPONENT
                                sample_rate: number
                                *delay_correction: number*
                            *preamplifier:*
                                *<< GENERIC_COMPONENT*
                            sensor:
                                << GENERIC_COMPONENT
                                 seed_codes:
                            *preamplifier_configuration: string*
                            *sensor_configuration: string*
                            *datalogger_configuration: string*
                            *location_code: string* # otherwise inherits from station
                            *comments: []*
                            *extras: {}*
                       <SPECIFIC-CHANNEL1>: {}
                       <SPECIFIC-CHANNEL2>: {}
                       ...
                *channel_modifications: {}*
                locations: {}
                *notes: []*
                *comments: []*
                *extras: {}*
                *processing:*
                    - *clock_correction_linear: {}*
                    - *clock_correction_leapsecond: {}*
            <STATIONNAME2>:
                ...

Where ``GENERIC_COMPONENT`` is:

.. code-block:: yaml

    equipment: 
        <equipment>
    *configuration_default: string*
    *configuration_definitions: {}*
    *response_modifications: {}*
    *notes: []*
    *response_stages:*
        - stage:
            input_units: string
            output_units string
            gain: float
            *name: string*
            *description: string*
            *decimation_factor: integer*
            *delay: number*
            *calibration_date: string*
            *polarity: '+' or '-'*     # default is '+'
            *input_sample_rate: number*
            *filter:*
                <filter>
        - stage:
        - ...