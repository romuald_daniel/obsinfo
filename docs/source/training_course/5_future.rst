.. _training_course_5:

**********************************************
Future plans
**********************************************


obsinfo v0.111
=========================================================================
All of this are "issues" on the gitlab site.  All will require changes to input files (major change)

Replace "network" with "subnetwork"
-----------------------------------------------------------
Avoid confusion with FDSN network, more compatible with concept of multiple facilities/deployments possibly composing an FDSN network

Bring instrument modifiers under `station:instrumentation`
-----------------------------------------------------------
And allow instrumentation-level configuration by adding ``modifications`` and ``config`` fields.

- This could also just be ``equipment_modifications`` as I think equipment is all that is modified
- ``serial_number`` is shorthand for ` (``serial_number`` is in ``equipment``), or maybe keep it as a shorthand
  for ``equipment_modifications {serial_number: }``: declaring both should be considered an error (can it be done in
  the schema file?)

**v0.110**

.. code-block:: yaml

    instrumentation:
         {$ref: "xxx.instrumentation.yaml"}
    serial_number: string
    channel_modifications: object

**0.111**

.. code-block:: yaml

  instrumentation:
      base: {$ref: "xxx.instrumentation.yaml"}
      serial_number: string
      config: string
      modifications: {equipment: object of equipment parameters to change}
      channel_modifications: object

or 

.. code-block:: yaml

  instrumentation:
      base: {$ref: "xxx.instrumentation.yaml"}
      serial_number: string
      config: string
      equipment_modifications: object of equipment parameters to change
      channel_modifications: object

Make "person" fields compatible with StationXML Person
-----------------------------------------------------------
Currently "author", change to "person"

**v0.110**

.. code-block:: yaml

    first_name: string
    last_name: string
    institution: string
    email: string
    phones: array of strings or [country_code, area_code, phone_number] objects

**0.111**

.. code-block:: yaml

    name: string
    agency: string
    email: string
    phone: string or [country_code, area_code, phone_number] objects

Make uncertainties into objects with measurement_method
-----------------------------------------------------------
Currently, azimuth.deg and dip.deg are 2-element arrays of number or null, with the first value being the value and the second the uncertainty, e.g.:

.. code-block:: yaml

    azimuth.deg: [90, 5]
    dip.deg: [0, 1]

Change them to be objects with the possible attributes specifed in the `StationXML Documetation<http://docs.fdsn.org/projects/stationxml/en/latest/reference.html#azimuth>`_:

.. code-block:: yaml

    azimuth: {value.deg: 90, uncert.deg: 5, measurement_method: "Ppol"}
    dip: {value.deg: 0, uncert.deg: 1, measurement_method: "Ppol"}

only `value.deg` should be required.

Should probably enable this for all fields that are defined as "FloatType" in the 1.1 XML schema:

- `WaterLevel` (units=m)
- `ClockDrift` (units=s?)
- `Response:Amplitude` (no units, specifed elsewhere?)
- `Response:Phase` (units=deg)
- `Response:Frequency` (units=Hz)
- `Decimation:InputSampleRate` (units=Hz)
- `PzTransferFunctionType:NormalizationFrequency` (units=Hz)
- `ApproximationType:FrequencyLowerBound` (units=Hz)
- `ApproximationType:FrequencyUpperBound` (units=Hz)
- `Decimation:Delay` (units=s)
- `Decimation:Correction` (units=s)
- `DipType` (units=deg)
- `Depth` (units=m)
- `SampleRate` (units=sps)

But allow value-only shortcut definitions, in which case `uncert` and `measurement_method` will be set to `null`, for exampe `WaterLevel.m: 0`,  `ClockDrift.s: 1e-10` and so on...
 
`Latitude.deg` `Longitude.deg` and `Elevation.m` are special cases for which the uncertainties and measurement method can be separately specified, as is currently the case.  

Or should I allow a separate _uncert variable for each field, in case they are the same for all instances (would only be inserted if the value was otherwise null)?

Perhaps have special `uncertainties` and `measurement_methods` objects that defined default uncertainties:

.. code-block:: yaml

    uncertainties: {lat.m: 100, lon.m: 100, elevation.m: 10}
    measurement_methods: {lat: "acoustic survey", lon: "acoustic survey", elevation: "acoustic survey", azimuth: "Ppol"}

for which I should probably keep the separation of uncertainties, measurement method and values

Remove intrumentation:operator field from schema
-----------------------------------------------------------
Already does nothing, but I kept it there to avoid breaking existing files

Make "operator" conform to StationXML standard
-----------------------------------------------------------
Currently flat:

.. code-block:: yaml

    operator:
        reference_name:  # Just for link with campaign file, not StationXML
        full_name:   # Change to "agency"
        contact:    # Currently just a person's name
        phone_number:
        email:
        website: 

Should be

.. code-block:: yaml

    operator:
        reference_name:  # Just for link with campaign file, not StationXML
        agency:
        contact:
            name:
            agency:
            phone_number:
            email:
        website: 

The contact is a Person type and so can just be loaded from an "author" file (change "authors" to "persons")

StationXML errors to fix in new version 0.111
-----------------------------------------------------------
The following errors, originally 6.3, 6.4, 8, 9, 10 and 12 in issue #3, will be addressed in v0.111.  Most require a new version, because we will need to add a `base` field to the `instrumentation` object in
order to bring station serial number and channel_modifications into this object:

1. No Serial Number shown for Station/Equipment
#. **LS5a is listed as 125 sps even though the network file says 62.5 sps**

And the following (possibly derived) errors:

1. Channel

    a) Sensor serial number is given as "32793N" (direct from instrumentation files, doesn't take into
       account OBS serial number)

#. Preamp/Datalogger/Equipment have no Serial Number

Moreover, I have to decide how to specify instrumentation-level serial numbers and configurations.  Should I imitate `channel_modifications` with and `instrumentation_modifications` (or just `modifications`?) field, or should I allow specific common fields such as `serial_number` and `instrumentation_configuration` (or `configuration`)? 

Other bugs that I didn't fix in v0.110.12:

1. Channel

    1. Sensor

        1. Has installation date, removal date and three calibration dates, all after expt
           (Trillium T240)
            
    #. PreAmplifier

        1. BBOBS gain card description is not specific enough (1x?  0.225x?)

#. No Comment (or field) saying how the station was located
#. Equipment description does not include configuration-specific information (need
   `configuration_description` field?)

**Should also make all tests work and maybe put my own tests back in**

Generalize base-configuration-modification
-----------------------------------------------------------
schema files currently have no means to be configured (or modified?).

One solution would be to add to this level.

Another would to allow base, configuration and modification fields at any level: if "base" is specified, then configuration and or modification can be put at the same level.
