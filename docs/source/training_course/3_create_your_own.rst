.. _training_course_2:

***********************************************************************
Creating a StationXML file using your own instruments and deployments
***********************************************************************

In each case, use:

- associated example and schema files to help you create the file(s)
- ``obsinfo-validate`` to validate your file(s)
- ``obsinfo-print`` for something?
- ``obsinfo-makeStationXML`` to verify that you can create a StationXML file

Creating a network file (using the example instruments)
===============================================================
Create your own network file, using existing instruments in the example_directory.

First-level associated files: ``network``
Second-level associated files: ``author``, ``network-info``, ``operators``,
                               ``location_bases``

Adding your own sensor/datalogger/analog filter
===============================================================
Add and validate sensor, datalogger and/or preamplifier(s) in the example_directory.
Modify an example instrumentation file to reference the new compoent(s) and
create a StationXML file

First-level associated files: ``datalogger``, ``sensor``, ``preamplifier``,
                              ``stage``, ``filter``
Second-level associated files: ``author``

Adding your own instrumentation
===============================================================
Create a new instrumentation (OBS), using your newly created components and
any other specific information

First-level associated files: ``instrumentation``
Second-level associated files: ``author``, ``operators``

Putting it all together
===============================================================
Create a network file corresponding to a deployment of your own instruments.
