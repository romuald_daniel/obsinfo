.. _training_course_2:

**********************************************
Setting up
**********************************************

Installation
=====================================================
Follow the instructions in the [Installation and Startup Guide], up to [Description of obsinfo file structure].
**Note that we have no Windows verification/development system, so Linux or MacOS systems are preferred (or someone
who will verify/modify the code for Windows)**


Copying an example database into your folder
=====================================================
run ``obsinfo-setup -d example_dir`` to create an directory containing a full set of example information files

Creating a StationXML file using the example database
=====================================================

- go into ``example_dir/Information/Files/network``
    - you should see several *.network.yaml files, including SPOBS.INSU-IPGP.network.yaml
- type ``obsinfo-makeStationXML SPOBS.INSU-IPGP.network.yaml``

if all is in order, a valid StationXML file named ``SPOBS.INSU-IPGP.station.xml`` should be created.  An easy way to verify is to try to read into obspy, like this:

.. code-block:: python
    from obspy.core.inventory import read_inventory

    inv = read_inventory('SPOBS.INSU-IPGP.station.xml', format='STATIONXML')
    print(inv)

Note that obsinfo-makeStationXML creates one StationXML file per station.
