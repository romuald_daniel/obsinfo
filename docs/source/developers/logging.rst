****************
Logging
****************

This is a stub.

This file should explain how information is logged (results of all error
checks, possibility of adding debugging levels...).  I assume we will use
Python's intrinsic ``logging`` module (already started, but I think
only for writing to screen).  Could take example from ppol, where I think
I do fairly careful, multi-leel logging.
Have to be careful to specify how to setup logging in each command-line
program (I assume there will be a standard, automatic level and then each
one can activate a more active debugging)