****************
Fundamentals
****************

This is a stub.

Explain here the fundamentals of obsinfo coding:
    - Need to follow StationXML as much as possible
        - But to add other fields
        - And to eliminate redundancy
    - Correlation and non-correlation of classes to StationXML objects
      and why
    - How an obsinfo file is parsed to obtain an obspy **Network** object
        - Including non-standard field stuffing into comments.