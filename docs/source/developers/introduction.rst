****************
Introduction
****************

The sections here concern the coding of some of the more complicated aspects
of obsinfo, to help with future modifications and debugging:

:delay_correction:
    explains how delay, offset, and delay_correction are implemented and
    related.  This is actually a fairly simple concept but the basic principals
    must not be lost/forgotten.

:channel_modification:
    This is one of the most important/complicated sections because it allows
    the user to completely modify an instrumentation from the station level.
    Uses ObsMetadata
    
:file_discovery:
    File discovery allows people to put their databases where they want, even
    on the web.  I don't know what the relationship is between the web
    address specified in the ``datapath`` parameter and the specific ``gitlab_*``
    parameters, need to figure it out.

:ObsMetadata:
    A subclass of ``dict`` that allows one to change any value using a specified
    parallel dict, without modifying other values.  Used for channel_modification,
    but I think also for other stuff.
    
    This class also has all of the information file reading, validating and
    identifying, which I think should maybe belong to another class
    
:testing:
    An overview of the ``unittest`` files used to stabilize/verify the results
    when the code is updated