This isn't really a test suite, but rather an evaluation of the current
infofile database (?, it seemed to test the locally installed database
referred to in .obinforc rather than the obsinfo/_examples database).

## Stages of work

- Remamed test.py as `run_test_script.py` so that it can still be called
  by `obsinfo-test` and `obsinfo-print`, but not discovered by
  `python -m unittest`
  
- Split test.py into unittest discoverable units:
    - `test_jsonref.py`: json ref tests, relatively fast, moved up to
      top-level `tests/`
    - `test_datapath.py`: should only test an information file database.
      I want to change this so it can EITHER test the datapath or the
      _examples/, and so that it can be called by run_test_script.py.
      
    - `test_infofile.py`: should only contain specific tests of infofile
      reading and evaluation, using in-code "info-files" as well as ones in the
      data/ directory.
        - I 

## Untangling test_datapath and test_infofile

The functions for `test_datapath.py` and `test_infofile.py` are intertwined, so
there is currently a lot of overlap between these codes: need to do the work
to correctly separate them.  Here's what I've done so far:

- test_infofile.py:
    - removed obvious datapath test routines (test_dp_*())
       
- test_datapath.py:
    - removed test_test_filters() and test_PZ_conditionals()
